module imem #(parameter N = 32)
					(input logic [6:0] addr,
					output logic [N-1:0] q);
					
	logic [N-1:0] rom [0:127] = '{32'hf8000001, 32'hf8008002, 32'hf8000203, 32'h8b050083,
											32'h8b1f03ff, 32'h8b1f03ff, //stall 2 cycles
											32'hf8018003, //stur x3
											32'hcb050083, //sub
											32'h8b1f03ff, 32'h8b1f03ff, //stall 2 cycles
											32'hf8020003, //stur x3 needs stall
											32'hcb0a03e4,
											32'h8b1f03ff, 32'h8b1f03ff, //stall 2 cycles
											32'hf8028004, //stur x4 needs stall
											32'h8b040064, //add x4
											32'h8b1f03ff, 32'h8b1f03ff, //stall 2 cycles
											32'hf8030004, //stur x4 AGAIN needs stall
											32'hcb030025, 32'h8b1f03ff, 32'h8b1f03ff,  //sub x5 and stall till wb
											32'hf8038005, //stur x5
											32'h8a1f0145, 32'h8b1f03ff, 32'h8b1f03ff, //and x5 and 2 stalls
											32'hf8040005, //stur x5 again
											32'h8a030145, 32'h8b1f03ff, 32'h8b1f03ff, //and x5 again and 2 stalls
											32'hf8048005, //stur x5 yet again
											32'h8a140294, 32'h8b1f03ff, 32'h8b1f03ff, //and x20 and 2 stalls
											32'hf8050014, //stur x20
											32'haa1f0166, 32'h8b1f03ff, 32'h8b1f03ff, //orr x6 and 2 stalls
											32'hf8058006, //stur x6
											32'haa030166, 32'h8b1f03ff, 32'h8b1f03ff, //orr x6 again and 2 stalls
											32'hf8060006, //stur x6 again
											32'hf840000c, 32'h8b1f03ff, 32'h8b1f03ff, //ldur x12 and 2 stall
											32'h8b1f0187, 32'h8b1f03ff, 32'h8b1f03ff, //add x7 using x12 and 2 stall
											32'hf8068007, //stur x7
											32'hf807000c, //stur x12
											32'h8b0e01bf, 32'hf807801f, //attempting to overwrite xzr and stur xzr
											32'hb40000a0, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, // cbz and stall for correct jump
											32'hf8080015, // wrong branch stur x21
											32'hf8088015, // right branch stur x21
											32'h8b0103e2, 32'h8b1f03ff, 32'h8b1f03ff, // add x2 and stall
											32'hcb010042, // sub x2
											32'h8b0103f8, 32'h8b1f03ff, 32'h8b1f03ff, // add x24 and stall
											32'hf8090018, //stur x24
											32'h8b080000, //add x0
											32'hb4ffff42, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, //cbz and stall
											32'hf809001e, //stur x30
											32'h8b1e03de, //add x30
											32'hcb1503f5, //negate x21
											32'h8b1f03ff, //stall
											32'h8b1403de, 32'h8b1f03ff, 32'h8b1f03ff, //add x30 and stall
											32'hf85f83d9, //ldur x25
											32'h8b1e03de, 32'h8b1f03ff, 32'h8b1f03ff, //add x30 and stall
											32'h8b1003de, 32'h8b1f03ff, 32'h8b1f03ff, //add x30 and stall
											32'hf81f83d9, //stur x25
											32'hb400001f, //infinite loop
											32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff,
											32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff,
											32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff,
											32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff,
											32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff,
											32'hb400001f, 32'h8b1f03ff};

// PROGRAMA SIN INSTRUCCIONES NOP
/*	logic [N-1:0] rom [0:127] = '{32'hf8000001, 32'hf8008002, 32'hf8000203, 32'h8b050083, 32'hf8018003, 32'hcb050083,
										32'hf8020003, 32'hcb0a03e4, 32'hf8028004, 32'h8b040064, 32'hf8030004, 32'hcb030025,
										32'hf8038005, 32'h8a1f0145, 32'hf8040005, 32'h8a030145, 32'hf8048005, 32'h8a140294,
										32'hf8050014, 32'haa1f0166, 32'hf8058006, 32'haa030166, 32'hf8060006, 32'hf840000c,
										32'h8b1f0187, 32'hf8068007, 32'hf807000c, 32'h8b0e01bf, 32'hf807801f, 32'hb4000040,
										32'hf8080015, 32'hf8088015, 32'h8b0103e2, 32'hcb010042, 32'h8b0103f8, 32'hf8090018,
										32'h8b080000, 32'hb4ffff82, 32'hf809001e, 32'h8b1e03de, 32'hcb1503f5, 32'h8b1403de,
										32'hf85f83d9, 32'h8b1e03de, 32'h8b1003de, 32'hf81f83d9, 32'hb400001f, 32'h8b1f03ff,
										32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff,
										32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff,
										32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff,
										32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff,
										32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff,
										32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff,
										32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff,
										32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff,
										32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff,
										32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff, 32'h8b1f03ff};*/

	assign q = rom[addr];
	
endmodule
