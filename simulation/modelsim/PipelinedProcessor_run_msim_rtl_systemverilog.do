transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/laboratoriouno {C:/Users/Franco/Documents/quartus/laboratoriouno/signext.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/laboratoriouno {C:/Users/Franco/Documents/quartus/laboratoriouno/regfile.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/laboratoriouno {C:/Users/Franco/Documents/quartus/laboratoriouno/maindec.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/laboratoriouno {C:/Users/Franco/Documents/quartus/laboratoriouno/imem.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/laboratoriouno {C:/Users/Franco/Documents/quartus/laboratoriouno/flopr.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/laboratoriouno {C:/Users/Franco/Documents/quartus/laboratoriouno/fetch.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/laboratoriouno {C:/Users/Franco/Documents/quartus/laboratoriouno/execute.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/laboratoriouno {C:/Users/Franco/Documents/quartus/laboratoriouno/alu.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/laboratoriouno {C:/Users/Franco/Documents/quartus/laboratoriouno/writeback.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/laboratoriouno {C:/Users/Franco/Documents/quartus/laboratoriouno/sl2.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/laboratoriouno {C:/Users/Franco/Documents/quartus/laboratoriouno/processor_arm.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/laboratoriouno {C:/Users/Franco/Documents/quartus/laboratoriouno/mux2.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/laboratoriouno {C:/Users/Franco/Documents/quartus/laboratoriouno/memory.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/laboratoriouno {C:/Users/Franco/Documents/quartus/laboratoriouno/decode.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/laboratoriouno {C:/Users/Franco/Documents/quartus/laboratoriouno/datapath.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/laboratoriouno {C:/Users/Franco/Documents/quartus/laboratoriouno/controller.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/laboratoriouno {C:/Users/Franco/Documents/quartus/laboratoriouno/aludec.sv}
vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/laboratoriouno {C:/Users/Franco/Documents/quartus/laboratoriouno/adder.sv}
vcom -2008 -work work {C:/Users/Franco/Documents/quartus/laboratoriouno/dmem.vhd}

vlog -sv -work work +incdir+C:/Users/Franco/Documents/quartus/laboratoriouno {C:/Users/Franco/Documents/quartus/laboratoriouno/processor_tb.sv}

vsim -t 1ps -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L altera_lnsim_ver -L cycloneive_ver -L rtl_work -L work -voptargs="+acc"  processor_tb

add wave *
view structure
view signals
run -all
