module fetch #(parameter N = 64)
					(input logic PCSrc_F, clk, reset,
					input logic [63:0] PCBranch_F,
					output logic [63:0] imem_addr_F);
					

	logic [63:0] alu_result, selected_instr, instr_addr;

	mux2 #(64) nexti_or_branch(.d0(alu_result), .d1(PCBranch_F),
										.s(PCSrc_F),
										.y(selected_instr));

	flopr #(64) program_counter(.clk(clk), .reset(reset), 
										.d(selected_instr),
										.q(instr_addr));
	

	adder #(64) nexti_addr(.a(instr_addr),
									.b(64'd4),
									.y(alu_result));
	

	assign imem_addr_F = instr_addr;
	
endmodule
