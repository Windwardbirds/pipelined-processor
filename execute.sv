module execute #(parameter N = 64)
					(input logic AluSrc,
					input logic [3:0] AluControl,
					input logic [63:0] PC_E, signImm_E, readData1_E, readData2_E,
					output logic [63:0] PCBranch_E, aluResult_E, writeData_E,
					output logic zero_E);
					

	logic [63:0] shifted_imm, selected_rm;

	sl2 #(64) shift_imm(.a(signImm_E),
								.y(shifted_imm));

	adder #(64) compute_branch_addr(.a(PC_E), .b(shifted_imm),
												.y(PCBranch_E));
	
	mux2 #(64) data_or_signimm(.d0(readData2_E), .d1(signImm_E),
										.s(AluSrc),
										.y(selected_rm));
	
	alu actual_alu(.a(readData1_E), .b(selected_rm),
						.ALUControl(AluControl),
						.result(aluResult_E),
						.zero(zero_E));
	

	assign writeData_E = readData2_E;
	
endmodule
